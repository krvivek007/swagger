'use strict';
var util    = require('util');
var mongoose = require('mongoose');
var Question = require('../models/question');
module.exports = {
   getAllQuestions: getAllQuestions,
   createQuestion:  createQuestion
};

function createQuestion(req, res) {
	var questsParams = req.swagger.params.questions.value;
	questsParams.forEach(function(aQuestion) {
		var question = Question({
			  Description: aQuestion.Description,
			  Options: aQuestion.Options,
			  Multiple: aQuestion.Multiple,
			  StartDate: aQuestion.StartDate,
			  EndDate: aQuestion.EndDate
			});
	    console.log(question);
	    question.save(function (err, savedObject) {
			  if (err) {
			    console.log(err);
			    res.json('error saving questions');
			  } else {
			    console.log('saved successfully:', savedObject);
			    res.json('saved successfully');
			  }
			});
	});
	
	
}

function getAllQuestions(req, res) {
	Question.find({}, '-_id -__v', function(err, questions) {
		console.log(questions);
	    res.json(questions);  
	  });
}



/*

var question1 = new Question({Description: 'modulus admin1', Options: ['admin', 'moderator', 'user'], Multiple: true});
console.log(question1);

//Lets save it
question1.save(function (err, savedObject) {
  if (err) {
    console.log(err);
  } else {
    console.log('saved successfully:', savedObject);
  }
});

console.log("saved.."); */

