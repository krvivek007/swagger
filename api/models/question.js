var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect(
		'',
		function(err) {
			if(err)
			console.log("Unable to connect to mongodb error:" + err);
		});

// create a schema
var questionSchema = new Schema({
	Description : String,
	Options : Array,
	Multiple : Boolean,
	StartDate : Date,
	EndDate : Date,
	created_at : Date,
	updated_at : Date
});
var Question = mongoose.model('Question', questionSchema);
questionSchema.methods.toJSON = function() {
	  var obj = this.toObject();
	  delete obj._id;
	  return obj;
	};
module.exports = Question;
